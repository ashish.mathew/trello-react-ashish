import "./App.css";

import "bootstrap/dist/css/bootstrap.css";
import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";



import BoardsContainer from "./components/boards-container/boards-container.component";
import NavBar from "./components/navBar/navBar.component";
import CardListsComponent from "./components/card-lists/card-list-container/card-container.component";


function App() {
  return (
    <Router>
    <React.Fragment>
    <NavBar />
    <Switch>
    <Route path="/" exact component={BoardsContainer} />
    <Route path="/boards:id" component={CardListsComponent} />
    </Switch>
    </React.Fragment>
    </Router>
    );
}

export default App;
