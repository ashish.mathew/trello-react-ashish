export let deleteComponent = (id,collections) => {
    return collections.filter(collection => collection.id !== id);
}