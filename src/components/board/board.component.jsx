import React from "react";

import { Link } from "react-router-dom";


const Board = (props) => {
  return (
    <Link to={`/boards${props.board.id}`} className="text-decoration-none" >
      <div className="col mb-4 rounded" style={{width:"16.8rem",height:"11rem",color:"white"}}>
        <div className="card" style={{height:"11rem",backgroundColor:`${props.board.prefs.background}`}}>
          <img
            src={props.board.prefs.backgroundImage}
            className="card-img"
            alt=""
          />
          <div className="card-img-overlay">
            <h5 className="card-title">{props.board.name}</h5>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default Board;
