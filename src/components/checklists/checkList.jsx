import React, { Component } from "react";

import { Card, ProgressBar, Button } from "react-bootstrap";

import { deleteComponent } from "../helperFunction-components/delete-component";
import CheckListItem from "./checkListItem-Component";
import CreateCheckListItem from "./createCheckListItem-component";

class CheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedItemCount:
        this.props.checkListDetails.checkItems.filter(
          (items) => items.state === "complete"
        ).length || 0,
      items: this.props.checkListDetails.checkItems,
      progressNow: 0,
    };
  }

  handleCheck = (item) => {
    let state = item.state;
    let value = this.state.checkedItemCount;
    value = state === "complete" ? ++value : --value;
    fetch(
      `https://api.trello.com/1/cards/${this.props.cardId}/checklist/${this.props.checkListDetails.id}/checkItem/${item.id}?state=${state}&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "PUT",
      }
    );
    this.setState({ checkedItemCount: value }, () => this.handleProgressBar());
  };

  handleProgressBar = () => {
    let progressNow = this.state.progressNow;
    progressNow = (this.state.checkedItemCount / this.state.items.length) * 100;
    this.setState({ progressNow });
  };

  handleAddItem = (itemName) => {
    let items = [...this.state.items];
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checkListDetails.id}/checkItems?name=${itemName}&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "POST",
      }
    )
      .then((response) => response.json())
      .then((newItem) => items.push(newItem))
      .then(() => this.setState({ items }, () => this.handleProgressBar()));
  };

  handleDeleteItem = (itemId) => {
    let items = [...this.state.items];
    fetch(
      `https://api.trello.com/1/checklists/${this.props.checkListDetails.id}/checkItems/${itemId}?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "DELETE",
      }
    )
      .then(() => deleteComponent(itemId, items))
      .then((items) => this.setState({ items }));
  };

  componentDidMount() {
    this.handleProgressBar();
  }

  render() {
    return (
      <>
        <Card.Header>
          {this.props.checkListDetails.name}
          <Button
            size="sm"
            variant="light"
            style={{ position: "absolute", right: "3%" }}
            onClick={() => this.props.onDelete(this.props.checkListDetails.id)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="red"
              className="bi bi-trash"
              viewBox="0 0 16 16"
            >
              <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" />
              <path
                fillRule="evenodd"
                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
              />
            </svg>
          </Button>
        </Card.Header>
        <Card.Body>
          <ProgressBar variant="success" now={this.state.progressNow} />
          {this.state.items.map((item) => (
            <CheckListItem
              key={item.id}
              item={item}
              onCheck={this.handleCheck}
              onDelete={this.handleDeleteItem}
            />
          ))}
          <CreateCheckListItem onAdd={this.handleAddItem} />
        </Card.Body>
      </>
    );
  }
}

export default CheckList;
