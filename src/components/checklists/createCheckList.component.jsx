import React, { Component } from "react";

import { Form, Card, Button } from "react-bootstrap";

class CreateCheckList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkListName: "",
    };
  }
  render() {
    return (
      <Card style={{ display: `${this.props.show}` }}>
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            this.props.onAdd(this.state.checkListName);
          }}
        >
          <Form.Control
            type="text"
            placeholder="checklist name"
            onChange={(e) => this.setState({ checkListName: e.target.value })}
          />

          <Button
            type="submit"
            variant="primary"
            // onClick={() => this.props.onAdd(this.state.checkListName)}
          >
            add
          </Button>
          <Button variant="light" onClick={this.props.onHide}>
            x
          </Button>
        </Form>
      </Card>
    );
  }
}

export default CreateCheckList;
