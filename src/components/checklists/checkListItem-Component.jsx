import React, { Component } from "react";

import { Form, Button } from "react-bootstrap";

class CheckListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.item.state === "complete" ? true : false,
    };
  }

  updateCheck = () => {
    let checked = !this.state.checked;
    this.props.item.state = checked === true?"complete":"incomplete";
    this.setState({ checked }, () => {
      this.props.onCheck(this.props.item);
    });
  };

  render() {
    return (
      <div style={{ position: "relative" }}>
        <Form.Check
          type="checkbox"
          id={this.props.item.id}
          label={this.props.item.name}
          onChange={this.updateCheck}
          checked={this.state.checked}
        />
        <Button
          variant="white"
          size="sm"
          style={{ position: "absolute", right: "1%", top: "-3%" }}
          onClick={() => this.props.onDelete(this.props.item.id)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="15"
            height="15"
            fill="red"
            className="bi bi-x"
            viewBox="0 0 13 13"
          >
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
          </svg>
        </Button>
      </div>
    );
  }
}

export default CheckListItem;
