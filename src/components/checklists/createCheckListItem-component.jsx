import React, { Component } from "react";
import { Button, Form } from "react-bootstrap";

class CreateCheckListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemName: "",
    };
  }
  render() {
    return (
      <>
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            this.props.onAdd(this.state.itemName);
          }}
        >
          <Form.Control
            type="text"
            placeholder="add new item"
            onChange={(e) => this.setState({ itemName: e.target.value })}
          />
          <Button
            type="submit"
            variant="light"
            value="Submit"
            // onClick={() => this.props.onAdd(this.state.itemName)}
          >
            add item
          </Button>
          
        </Form>
      </>
    );
  }
}

export default CreateCheckListItem;
