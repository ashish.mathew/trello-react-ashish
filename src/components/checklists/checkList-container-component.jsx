import React, { Component } from "react";

import { Button, Modal } from "react-bootstrap";

import CheckList from "./checkList";
import CreateCheckList from "./createCheckList.component";
import { deleteComponent } from "../helperFunction-components/delete-component";

class CheckListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkLists: [],
      show: ["none", "block"],
    };
  }

  handleShowAndClose = () => {
    this.setState({ show: [...this.state.show].reverse() });
  };

  // handleShowForm = () => {
  //   this.setState({ showForm: true });
  // };
  // handleCloseForm = () => {
  //   this.setState({ showForm: false });
  // };
  handleAdd = (checkListName) => {
    let checkLists = [...this.state.checkLists];
    fetch(
      `https://api.trello.com/1/checklists?name=${checkListName}&idCard=${this.props.cardId}&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "POST",
      }
    )
      .then((response) => response.json())
      .then((newCheckList) => checkLists.push(newCheckList))
      .then(() => this.setState({ checkLists }));
    this.handleShowAndClose();
  };

  handleDeleteCheckList = (checkListId) => {
    let checkLists = [...this.state.checkLists];
    fetch(
      `https://api.trello.com/1/checklists/${checkListId}?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "DELETE",
      }
    )
      .then(() => deleteComponent(checkListId, checkLists))
      .then((checkLists) => this.setState({ checkLists }));
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/cards/${this.props.cardId}/checklists?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`
    )
      .then((response) => response.json())
      .then((checkLists) => this.setState({ checkLists }));
  }

  render() {
    return (
      <Modal centered show={this.props.show}>
        <Modal.Header>
          <Modal.Title>{this.props.cardName}</Modal.Title>
          <Button
            variant="info"
            onClick={this.props.onHide}
            style={{ borderRadius: "50%" }}
          >
            x
          </Button>
        </Modal.Header>
        <Modal.Body>
          {this.state.checkLists.map((eachchecklist) => (
            <CheckList
              key={eachchecklist.id}
              checkListDetails={eachchecklist}
              onDelete={this.handleDeleteCheckList}
              cardId={this.props.cardId}
            />
          ))}
          <div
            style={{
              position: "relative",
            }}
          >
            <Button
              onClick={this.handleShowAndClose}
              style={{ display: `${this.state.show[1]}` }}
            >
              add checklist
            </Button>
            <CreateCheckList
              show={this.state.show[0]}
              style={{ position: "absolute" }}
              onHide={this.handleShowAndClose}
              onAdd={this.handleAdd}
            />
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}

export default CheckListContainer;
