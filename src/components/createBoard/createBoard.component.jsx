import React from "react";
import { Button, Modal, Form } from "react-bootstrap";

class CreateBoard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      boardName: "",
    };
  }

  render() {
    return (
      <Modal
        show={this.props.show}
        
      >
        <Modal.Header>
          <Modal.Title>Create new board</Modal.Title>
        </Modal.Header>
          <Form onSubmit={(e) => {
          e.preventDefault();
          this.props.onCreate(this.state.boardName);
        }}>
        <Modal.Body>
          <Form.Control
            type="text"
            placeholder="Board name"
            onChange={(e) => this.setState({ boardName: e.target.value })}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={this.props.onHide}>
            cancel
          </Button>
          <Button
            type="submit"
            variant="primary"
            onClick={() => this.props.onCreate(this.state.boardName)}
            
          >
            create
          </Button>
        </Modal.Footer>
          </Form>
      </Modal>
    );
  }
}

export default CreateBoard;
