import React, { Component } from "react";

import Button from "react-bootstrap/Button";

import Board from "../board/board.component";
import CreateBoard from "../createBoard/createBoard.component";


class BoardsContainer extends Component {
  constructor() {
    super();
    this.state = {
      boards: [],
      show: false,
    };
  }

  handleClose = () => this.setState({ show: false });
  handleShow = () => this.setState({ show: true });

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/members/me/boards?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`)
      .then((response) => response.json())
      .then((myboards) => this.setState({ boards: myboards }));
  }
  addBoard = (boardName) => {
    let boards = [...this.state.boards];
    fetch(
      `https://api.trello.com/1/boards/?&name=${boardName}&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "POST",
      }
    )
      .then((response) => response.json())
      .then((newboard) => boards.push(newboard))
      .then(() => this.setState({ boards }));
    this.handleClose();
  };

  render() {
    return (
      <div>
        <h1 className="text-center my-5">My Boards</h1>
        <div className="container">
          <div className="row row-cols-1 row-cols-md-4">
            {this.state.boards.map((board) => (
              <Board key={board.id} board={board} />
            ))}
            <Button
              style={{ width: "16.8rem", height: "11rem" }}
              className="mx-2"
              variant="primary"
              onClick={this.handleShow}
            >
              create board
            </Button>
            <CreateBoard
              show={this.state.show}
              onHide={this.handleClose}
              onCreate={this.addBoard}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default BoardsContainer;
