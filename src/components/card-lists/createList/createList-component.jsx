import React from "react";
import { Button, Form, Card } from "react-bootstrap";

class CreateList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listName: "",
    };
  }

  render() {
    return (
      <Card className="my-2"
        style={{ display: `${this.props.show}`,minWidth:"10rem" }}
      >
        <Form onSubmit={(e) => {
          e.preventDefault();
          this.props.onCreate(this.state.listName)
        }}>
        <Form.Control
          size="sm"
          type="text"
          placeholder="list name"
          onChange={(e) => this.setState({ listName: e.target.value })}
        />
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-start",
          }}
        >
          <Button
          type="submit"
            size="sm"
            variant="primary"
            // onClick={() => this.props.onCreate(this.state.listName)}
          >
            add list
          </Button>
          <Button variant="light" onClick={this.props.onHide}>
            x
          </Button>
        </div>
        </Form>
      </Card>
    );
  }
}

export default CreateList;

