import React, { Component } from "react";

import CardList from "../card-list-component/cardList-compoinent";
import CreateList from "../createList/createList-component";
import { deleteComponent } from "../../helperFunction-components/delete-component";

import { Button } from "react-bootstrap";

class CardListsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boardLists: [],
      show: ["none", "block"],
    };
  }

  handleShowAndClose = () =>
    this.setState({ show: [...this.state.show].reverse() });

  // handleClose = () => this.setState({ show: "none" });
  // handleShow = () => this.setState({ show: "inline" });

  handleDelete = (listId) => {
    fetch(
      `https://api.trello.com/1/lists/${listId}?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}&closed=true`,
      {
        method: "PUT",
      }
    )
    .then(() => deleteComponent(listId, this.state.boardLists))
    .then((boardLists) => this.setState({ boardLists }));
  };

  addList = (listName) => {
    this.handleShowAndClose();
    let boardLists = [...this.state.boardLists];
    fetch(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${this.props.match.params.id}&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}&pos=bottom`,
      {
        method: "POST",
      }
    )
      .then((response) => response.json())
      .then((newList) => boardLists.push(newList))
      .then(() => this.setState({ boardLists }));
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/boards/${this.props.match.params.id}/lists?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`
    )
      .then((response) => response.json())
      .then((myBoardLists) => this.setState({ boardLists: myBoardLists }));
  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "flex-start",
          position: "relative",
        }}
      >
        {this.state.boardLists.map((list) => (
          <CardList key={list.id} list={list} onDelete={this.handleDelete} />
        ))}
        <div style={{ position: "relative", paddingRight: "10px" }}>
          <Button
            className="my-2"
            variant="secondary"
            onClick={this.handleShowAndClose}
            style={{ minWidth: "10rem", display: `${this.state.show[1]}` }}
          >
            + Add another list
          </Button>
          <CreateList
            className="my-2"
            show={this.state.show[0]}
            onHide={this.handleShowAndClose}
            onCreate={this.addList}
            style={{ position: "absolute", minWidth: "10rem" }}
          />
        </div>
      </div>
    );
  }
}

export default CardListsContainer;
