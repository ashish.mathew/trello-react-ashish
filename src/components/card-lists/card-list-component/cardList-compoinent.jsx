import React, { Component } from "react";
import { Button } from "react-bootstrap";

import Card from "../../cards/card-component";
import CreateCard from "../../cards/createCard-component";
import { deleteComponent } from "../../helperFunction-components/delete-component";

class CardList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCards: [],
      show: ["none", "block"],
    };
  }

  handleShowAndClose = () =>
    this.setState({ show: [...this.state.show].reverse() });

  // handleHide = () => {
  //   this.setState({ show: false });
  // };
  // handleShow = () => {
  //   this.setState({ show: true });
  // };

  handleCreate = (cardName) => {
    let listCards = [...this.state.listCards];
    fetch(
      `https://api.trello.com/1/cards?name=${cardName}&idList=${this.props.list.id}&key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "post",
      }
    )
      .then((response) => response.json())
      .then((newCard) => listCards.push(newCard))
      .then(() => this.setState({ listCards }));
    this.handleShowAndClose();
  };

  handleDeleteCard = (cardId) => {
    fetch(
      `https://api.trello.com/1/cards/${cardId}?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`,
      {
        method: "DELETE",
      }
    )
      .then(() => deleteComponent(cardId, this.state.listCards))
      .then((listCards) => this.setState({ listCards }));
  };

  componentDidMount() {
    fetch(
      `https://api.trello.com/1/lists/${this.props.list.id}/cards?key=${process.env.REACT_APP_API_KEY}&token=${process.env.REACT_APP_API_TOKEN}`
    )
      .then((response) => response.json())
      .then((listCards) => this.setState({ listCards }));
  }

  render() {
    return (
      <div
        className="card m-2 bg-light text-dark"
        style={{ minWidth: "17rem" }}
      >
        <div className="card-body" style={{ position: "relative" }}>
          <Button
            variant="danger"
            size="sm"
            style={{ position: "absolute", top: "1%", right: "1%" }}
            onClick={() => this.props.onDelete(this.props.list.id)}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="13"
              height="13"
              fill="currentColor"
              className="bi bi-archive"
              viewBox="0 0 16 16"
            >
              <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1V2zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5H2zm13-3H1v2h14V2zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5z" />
            </svg>
          </Button>

          <h6
            className="card-title"
            style={{ color: "black", bold: "600", fontSize: "1rem" }}
          >
            {this.props.list.name}
          </h6>

          {this.state.listCards.map((card) => (
            <Card key={card.id} card={card} onDelete={this.handleDeleteCard} />
          ))}
        </div>
        <div className="card-footer">
          <Button
            variant="light"
            size="sm"
            onClick={this.handleShowAndClose}
            style={{ display: `${this.state.show[1]}` }}
          >
            + add a card
          </Button>

          <CreateCard
            show={this.state.show[0]}
            onHide={this.handleShowAndClose}
            onCreate={this.handleCreate}
          />
        </div>
      </div>
    );
  }
}

export default CardList;
