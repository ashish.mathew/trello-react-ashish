import React, { Component } from "react";

import { Button, Card, Form } from "react-bootstrap";

import "./createCard-component.css";

class CreateCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      CardName: "",
    };
  }

  render() {
    return (
      <Card
        className="my-2"
        style={{ display: `${this.props.show}`, minWidth: "10rem" }}
      >
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            this.props.onCreate(this.state.CardName);
          }}
        >
          <Form.Control
            size="sm"
            type="text"
            placeholder="card name"
            onChange={(e) => this.setState({ CardName: e.target.value })}
          />
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-start",
            }}
          >
            <Button
              type="submit"
              size="sm"
              variant="primary"
              // onClick={() => this.props.onCreate(this.state.CardName)}
            >
              add card
            </Button>
            <Button variant="light" onClick={this.props.onHide}>
              x
            </Button>
          </div>
        </Form>
      </Card>
    );
  }
}

export default CreateCard;
